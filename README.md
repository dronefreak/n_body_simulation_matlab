# README #

Interpretation of the space dynamics using the N-Body Simulation. Study the patterns of galaxy formation.

### Setup ###

Clone the repo and find out yourself ! Run the code in MATLAB and see the results.
![](https://bytebucket.org/dronefreak/n_body_simulation_matlab/raw/6d8fb60729feac8b9e53b7e41b3b41f427fb9fd8/Orbital%20Phy.png)


### Contribution guidelines ###

* Analyze for more than 3 body galaxies
* Code review